#!/usr/bin/python3
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

APP="example"
DIR="locale"

import locale
locale.setlocale(locale.LC_ALL, '')
locale.bindtextdomain(APP, DIR)

import gettext
gettext.bindtextdomain(APP, DIR)
gettext.textdomain(APP)
_ = gettext.gettext

print(_("Hello, World!"))

def onBtnPortugueseClicked(button):
    print("Portuguese")
    locale.setlocale(locale.LC_ALL, 'pt_BR.utf8') # This line does nothing

def onBtnEnglishClicked(button):
    print("English")
    locale.setlocale(locale.LC_ALL, 'en_US.utf8') # This line does nothing
    
def onBtnCloseClicked(button):
    mainWindow.close()

builder = Gtk.Builder()
builder.set_translation_domain(APP)
builder.add_from_file("example.ui")

btnPortuguese = builder.get_object("btnPortuguese")
btnPortuguese.connect("clicked", onBtnPortugueseClicked)

btnEnglish = builder.get_object("btnEnglish")
btnEnglish.connect("clicked", onBtnEnglishClicked)

btnClose = builder.get_object("btnClose")
btnClose.connect("clicked", onBtnCloseClicked)

mainWindow = builder.get_object("mainWindow")
mainWindow.connect("destroy", Gtk.main_quit)
mainWindow.show()

Gtk.main()

